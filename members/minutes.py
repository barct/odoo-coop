# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################
from openerp import models, fields, api
#from datetime import date
from openerp.osv import fields as old_fields


class minutes(models.Model):
	book_number = fields.Integer(string='Book N°')
	number = fields.Integer(string='Minutes N°')
	date = fields.Date(string='Date')
